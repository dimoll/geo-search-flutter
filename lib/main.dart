import 'package:flutter/material.dart';

import 'ui/search_page.dart';

void main() => runApp(GeoSearchApp());

class GeoSearchApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Ubrainians',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SearchPage(),
    );
  }
}
