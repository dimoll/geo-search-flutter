class GeonamesResponse {
  List<Geonames> geonames;

  GeonamesResponse({this.geonames});

  factory GeonamesResponse.fromJson(Map<String, dynamic> json) {
    if (json['geonames'] != null) {
      List<Geonames> geonamesFromJson = new List<Geonames>();
      json['geonames'].forEach((v) {
        geonamesFromJson.add(new Geonames.fromJson(v));
      });
      return GeonamesResponse(geonames: geonamesFromJson);
    }
    return null;
  }
}

class Geonames {
  final String countryCode;
  final String name;
  final String toponymName;

  Geonames({
    this.countryCode,
    this.name,
    this.toponymName,
  });

  factory Geonames.fromJson(Map<String, dynamic> json) {
    return Geonames(
      countryCode: json['countryCode'],
      name: json['name'],
      toponymName: json['toponymName'],
    );
  }
}