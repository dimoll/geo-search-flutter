import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:ubrainians_geo_search/data/geonames_response.dart';

class SearchPage extends StatefulWidget {
  SearchPage({Key key}) : super(key: key);

  @override
  _SearchPageState createState() => new _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  Widget appBarTitle = new Text(
    "",
    style: new TextStyle(color: Colors.white),
  );
  Icon actionIcon = new Icon(
    Icons.search,
    color: Colors.white,
  );

  final key = new GlobalKey<ScaffoldState>();
  final TextEditingController _searchQuery = new TextEditingController();
  List<String> _geonamesList;
  bool _isSearching;
  String _searchText = "";

  List<String> _listSelectedValue;

  _SearchPageState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
          getNames(_searchText);
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _isSearching = false;
    createSearchResultList();
  }

  void createSearchResultList() {
    _geonamesList = <String>[];

    // this is initial data for Selected List View
    _listSelectedValue = <String>["Kyiv", "Moscow", "London"];
  }

  Future getNames(String q) async {
    final String url =
        'http://api.geonames.org/searchJSON?q=$q&maxRows=10&style=SHORT&username=dimoll';
    final response = await http.get(url);
    if (response.statusCode == 200) {
      final responseGeo = GeonamesResponse.fromJson(json.decode(response.body));
      setState(() {
        _geonamesList.clear();
        responseGeo.geonames.forEach((f) {
          _geonamesList.add(f.name);
        });
      });
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: key,
      appBar: buildBar(context),
      body: new Stack(
        children: <Widget>[
          new Container(
            padding: EdgeInsets.all(10.0),
            child: new Container(child: _listViewSelected()),
          ),
          displaySearchResults(),
        ],
      ),
    );
  }

  Widget displaySearchResults() {
    if (_isSearching) {
      return new Align(alignment: Alignment.topCenter, child: searchList());
    } else {
      return new Align(alignment: Alignment.topCenter, child: new Container());
    }
  }

  Widget _listViewSelected() {
    return new ListView.builder(
        itemCount: _listSelectedValue.length,
        itemBuilder: (BuildContext context, int index) {
          final item = _listSelectedValue[index];

          return Dismissible(
            key: Key(item),
            onDismissed: (direction) {
              setState(() {
                _listSelectedValue.removeAt(index);
              });

              _showDeletedToast(context, item);
            },

            background: Container(color: Colors.red),
            child: new Card(
              color: Colors.cyan[50],
              elevation: 5.0,
              child: new Container(
                width: double.infinity,
                margin: EdgeInsets.all(15.0),
                child: new Text("${_listSelectedValue[index]}"),
              ),
            ),
          );
        });
  }

  ListView searchList() {
    return ListView.builder(
      itemCount: _geonamesList.isEmpty == null ? 0 : _geonamesList.length,
      itemBuilder: (context, int index) {
        return Container(
          decoration: new BoxDecoration(
              color: Colors.grey[100],
              border: new Border(
                  bottom: new BorderSide(color: Colors.grey, width: 0.5))),
          child: ListTile(
            onTap: () {
              _handleSearchEnd();
              _listSelectedValue.insert(0, _geonamesList[index]);
              _showToast(context, _geonamesList[index]);
            },
            title: Text(_geonamesList.elementAt(index),
                style: new TextStyle(fontSize: 18.0)),
          ),
        );
      },
    );
  }

  void _showToast(BuildContext context, String s) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: Text('$s added to favorite'),
        action: SnackBarAction(
            label: 'UNDO',
            onPressed: () {
              _undoAddedOperation();
            }),
      ),
    );
  }

  Widget buildBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: appBarTitle,
      actions: <Widget>[
        new IconButton(
          icon: actionIcon,
          onPressed: () {
            _displayTextField();
          },
        ),
      ],
    );
  }

  void _displayTextField() {
    setState(() {
      if (this.actionIcon.icon == Icons.search) {
        this.actionIcon = new Icon(
          Icons.close,
          color: Colors.white,
        );
        this.appBarTitle = new TextField(
          autofocus: true,
          controller: _searchQuery,
          style: new TextStyle(
            color: Colors.white,
          ),
        );

        _handleSearchStart();
      } else {
        _handleSearchEnd();
      }
    });
  }

  void _showDeletedToast(BuildContext context, String name) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: Text('$name deleted'),
        action: SnackBarAction(
            label: 'HIDE', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  void _undoAddedOperation() {
    setState(() {
      _listSelectedValue.removeAt(0);
    });
  }

  void _handleSearchStart() {
    setState(() {
      _isSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      this.actionIcon = new Icon(
        Icons.search,
        color: Colors.white,
      );
      this.appBarTitle = new Text(
        "",
        style: new TextStyle(color: Colors.white),
      );
      _isSearching = false;
      _searchQuery.clear();
    });
  }
}
